/* NUR ANIS SYAMIMI BINTI ALI 2020859334
   29/6/2021 */

function validateForm(){

	var first = document.forms["quiz"]["first"].value;
	var second = document.forms["quiz"]["second"].value;
	var third = document.forms["quiz"]["third"].value;
	var forth = document.forms["quiz"]["forth"].value;
	var fifth = document.forms["quiz"]["fifth"].value;
	var sixth = document.forms["quiz"]["sixth"].value;
	var seventh = document.forms["quiz"]["seventh"].value;
	var eighth = document.forms["quiz"]["eighth"].value;
	var nineth = document.forms["quiz"]["nineth"].value;
	var tenth = document.forms["quiz"]["tenth"].value;
	var name = document.forms["quiz"]["name"].value;
	var score=0;

	if(first==""){
		alert("Oops!! Question 1 is required");
		return false;
	}

	if(second==""){
		alert("Oops!! Question 2 is required");
		return false;
	}

	if(third==""){
		alert("Oops!! Question 3 is required");
		return false;
	}

	if(forth==""){
		alert("Oops!! Question 4 is required");
		return false;
	}

	if(fifth==""){
		alert("Oops!! Question 5 is required");
		return false;
	}

	if(sixth==""){
		alert("Oops!! Question 6 is required");
		return false;
	}

	if(seventh==""){
		alert("Oops!! Question 7 is required");
		return false;
	}

	if(eighth==""){
		alert("Oops!! Question 8 is required");
		return false;
	}

	if(nineth==""){
		alert("Oops!! Question 9 is required");
		return false;
	}

	if(tenth==""){
		alert("Oops!! Question 10 is required");
		return false;
	}

	if(name==""){
		alert("Oops!! Your name is required");
		return false;
	}

	if(first=="Cascading Style Sheets")
	{
		score++;
	}

	if(second=="style")
	{
		score++;
	}

	if(third=="background-color")
	{
		score++;
	}

	if(forth=="color")
	{
		score++;
	}

	if(fifth=="font-size")
	{
		score++;
	}

	if(sixth=="font-weight:bold;")
	{
		score++;
	}

	if(seventh=="Separate each selector with a comma")
	{
		score++;
	}

	if(eighth=="static")
	{
		score++;
	}

	if(nineth=="#demo")
	{
		score++;
	}

	if(tenth=="No")
	{
		score++;
	}

	if(score<5)
	{
		alert("Keep trying, "+name+"! You answered "+score+" out of 10 correctly.");
	}
	else if(score<10)
	{
		alert("Way to go, "+name+"! You got "+score+" out of 10 correct.");
	}
	else
	{
		alert("Congratulations "+name+"! You got "+score+" out of 10.");
	}
}

